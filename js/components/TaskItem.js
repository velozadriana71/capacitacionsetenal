export const TaskItem = (id, title, description) =>
`
<div class="task">
            <div class="task-header">${title}</div>
            <div class="task-body">
                <p>${description}</p>
                <div class="task-actions">
                    <a href="/form.html#${id}" class="task-actions-btn btn" >Edit</a>
                    <button class="task-actions-btn btn" onclick={deleteData(${id})}>Delete</button>
                </div>
            </div>
        </div>
    </div>
`;